��    $      <  5   \      0  '   1  9   Y  g  �  -   �  +   )     U     j     �  "   �     �     �  /   �  #   *     N  ?   i  $   �  2   �  '     1   )  7   [  "   �     �  %   �     �          3     O     j     �  D   �     �     �        1   6     h  q  �  (   �  @     �  ^  :   K%  /   �%     �%  (   �%  %   �%  (    &  !   I&  #   k&  ;   �&  (   �&  %   �&  I   '  2   d'  B   �'  5   �'  B   (  B   S(  0   �(  "   �(  *   �(      )     6)     V)     v)     �)  %   �)  F   �)  #   *     C*  -   `*  6   �*     �*     !                                    	       
   #       "                                                                         $                                                 ${dir_or_empty} is not a directory path ${dir_or_empty} is not a directory path. Umounting anyway Prepare a zRAM device and use it as swap (resp. mount it under DIR).
SIZE is the maximal size in megabytes.
For umounting/freeing the zRAM device, use SIZE=0.
When using "write" (or anything else starting with "w") an idle writeback
is forced (only makes sense if previously initialized with "-w" or "-W LIMIT").
If DIR is - then only a filesystem is created in /dev/zram$DEV (or the device
is removed if SIZE is 0) but it is not mounted
(options "-o", "-c", "-m", and "-T" take no effect in this case, of course).
The latter can be useful e.g. for Btrfs if multiple devices should be mounted
jointly afterwards.
The following options are available.
An empty argument means the same as if the option is not specified.
-d DEV     Use zRAM device DEV. If not specified, DEV=0 is assumed.
           Make sure to use the matching value for umounting (SIZE=0)!
-D NUM     If modprobe needs to be used, require NUM devices. This is not
           recommended. Rely instead on /etc/modprobe.d/zram.conf with the line
           "options zram num_devices=NUM"
-s NUM     Use up to NUM parallel compression streams for the device
-S MAX     Use maximally MAX megabytes of uncompressed memory for the device
-b DEV     Use DEV as backing device
-I         If combined with "-b DEV", store incompressible pages to
           backing device
-w         If combined with "-b DEV", enable idle writeback to backing device
-W LIMIT   As "-w" but additionally set writeback_limit to LIMIT * 4kB.
-a ALGO    Set compression algorithm to ALGO (e.g. zstd, lz4, or lzo)
-c OWNER   If specified, chown to OWNER (resp. OWNER:GROUP) after mounting.
           If not specified, the default owner (root:root) is not changed.
-m MODE    If specified, chmod DIR to MODE after mounting.
-o OPTS    If specified, mount DIR with "-o OPTS".
-p PRIO    Use priority PRIO for the swap device.
           If not specified, PRIO=16383 is assumed.
           Use PRIO=- if you want to keep the default priority (-1).
-t TYPE    Use a filesystem of type TYPE if DIR is specified.
           TYPE can be either ext2, ext4, btrfs or xfs
           If not specified, TYPE=ext4 is assumed.
-B BSIZE   Override default blocksize (ext2, ext4) (BSIZE=1024|2048|4096)
-i IRATIO  If specified override default bytes/inodes in fs (ext2, ext4)
-N INODES  If specified override inode count (ext2, ext4)
-L LABEL   Specify the label LABEL for the new filesystem
-U UUID    Specify the uuid UUID for the new filesystem
-T         If specified, do not use the discard (TRIM) feature of ext4/swap.
           Use this option with linux-3.14 or earlier or when you want a slight
           speed increase at the cost of possibly wasting a lot of memory
-l         Do not use zramctl even if available
-k         Do no attempt to umount/free a previously used zRAM under this
           device

If you have push.sh in $PATH, you can also use accumulatively:
-K ARG     Pass ARG to the respective mkswap or mkfs.* call
-M ARG     Pass ARG to the respective swapon/mount call
-2 ARG     Pass ARG to the tune2fs call (ignored unless for ext2 or ext4)
-Z ARG     Pass ARG to the zramctl call

Call with LANG=C to disable translations Usage: ${BASENAME} [options] SIZE|write [DIR] cannot create ${devnode}: ${zclass} missing cannot find ${block} cannot set zram${dev} size device ${dev} is not numeric device count ${num} is not numeric failed to calculate size failed to create ${devnode} failed to enable writeback_limit for zram${dev} failed to idle writeback zram${dev} failed to reset zram${dev} failed to set writeback_limit ${writeback_limit} for zram${dev} failed to set zram${dev} backing_dev failed to set zram${dev} comp_algorithm to ${algo} failed to set zram${dev} idle writeback failed to set zram${dev} incompressible writeback failed to set zram${dev} max_comp_streams to ${streams} failed to set zram${dev} mem_limit hot_add failed for ${devnode} mem_limit ${mem_limit} is not numeric mkfs.btrfs ${devnode} failed mkfs.ext2 ${devnode} failed mkfs.ext4 ${devnode} failed mkfs.xfs ${devnode} failed mkswap ${devnode} failed mount ${devnode} failed push.sh from https://github.com/vaeth/push/ (v2.0 or newer) required size ${size} is not numeric swapon ${devnode} failed unsupported filesystem ${fstype} writeback_limit ${writeback_limit} is not numeric zramctl zram${dev} failed Project-Id-Version: zram-init
Report-Msgid-Bugs-To: Martin V"ath <martin@mvath.de>
PO-Revision-Date: 2021-03-18 19:39+0100
Last-Translator: Martin V"ath <martin@mvath.de>
Language-Team: German
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n >= 2);
X-Generator: Lokalize 19.12.3
 ${dir_or_empty} ist kein Verzeichnispfad ${dir_or_empty} ist kein Verzeichnispfad. Hänge es trotzdem aus Initialisiert ein zRAM-Device und benutzt es also Swap
(bzw. hängt es als VERZEICHNIS ein).
GRÖSSE ist die maximale Größe in Megabytes.
Zum Aushängen/Freigeben des zRAM-Devices kann man SIZE=0 benutzen.
Bei Benutzung von "write" (oder etwas anderem, das mit "w" beginnt) wird ein
idle writeback erzwungen. (Das ist nur nach vorheriger Initialisierung mit "-w"
oder "-W LIMIT" sinnvoll.)
Falls VERZEICHNIS ein - ist, so wird nur ein Dateisystem in /dev/zram$DEV
erzeugt (bzw. falls GRÖSSE 0 ist, wird das Device gelöscht), aber es wird
nicht eingehängt. (Die Optionen "-o", "-c", "-m" und "-T" haben in
diesem Fall natürlich keine Wirkung.)
Letzteres kann sinnvoll sein, wenn z.B. für Btrfs später mehrere Devices
gemeinsam eingehängt werden sollen.
Es gibt die folgenden Optionen.
Ein leeres Argument ist gleichbedeutend mit dem Weglassen der Option.
-d DEV     Nutzt zRAM device DEV. Wenn ncht angegeben wird DEV=0 genommen.
           Der selbe Wert muss auch zum Aushängen (GRÖSSE=0) benutzt werden!
-D NUM     Wenn modprobe benutzt werden muss, werden NUM Devices angefordert.
           Dies ist nicht empfohlen. Empfohlen ist statt dessen die Nutzung
           von /etc/modprobe.d/zram.conf mit der Zeile
           "options zram num_devices=NUM"
-s NUM     Nutzt bis zu NUM paralleler Kompressionsströme für das Device.
-S MAX     Nutzt maximal MAX Megabytes unkompromierten Speicher für das
           Device.
-b DEV     Nutzt DEV als Backing Device.
-I         Falls mit "-b DEV" kombiniert, speichert unkomprimierbare Seiten
           in das Backing Device.
-w         Falls mit "-b DEV" kombiniert, ermöglicht Idle Writeback für das
           Backing Device.
-W LIMIT   Wie "-w", aber zusätzlich wird writeback_limit auf LIMIT * 4kB
           gesetzt.
-a ALGO    Setzt Kompressions-Algorithmus auf ALGO (z.B. zstd, lz4, lzo).
-c OWNER   Falls angegeben, chown nach OWNER (bzw. OWNER:GROUP) nach dem
           Einhängen. Falls nicht angegeben wird der Default-Eigentümer
           (root:root) nicht verändert.
-m MODE    Falls angegeben, chmod VERZEICHNIS zu MODE nach dem Einhängen.
-o OPTS    Falls angegeben, hängt VERZEICHNIS mit "-o OPTS" ein.
-p PRIO    Nutzt Priorität PRIO für das Swap-Device.
           Falls nicht angegeben, wird PRIO=16383 genommen.
           Mit PRIO=- wird die voreingestellte Priorität (-1) beibehalten.
-t TYP     Nutzt ein Dateisystem vom Typ TYP, wenn VERZEICHNIS angegeben ist.
           Mögliche Werte für TYP sind ext2, ext4, btrfs und xfs
           Wenn nicht angegeben, wird TYPE=ext4 genommen.
-B BSIZE   Let Blockgröße fest (ext2, ext4) (BSIZE=1024|2048|4096)
-i IRATIO  Wenn angegeben, wird das Bytes/Inodes-Verhältnis im Dateisystem
           überschrieben (ext2, ext4).
-N INODES  Wenn angegeben, wird die Inode-Zahl überschrieben (ext2, ext4).
-L LABEL   Nimmt Label LABEL für das neue Dateisystem.
-U UUID    Nimmt Uuid UUID für das neue Dateisystem.
-T         Wenn angegeben, wird das Discard (TRIM) Feature von ext4/swap nicht
           genutzt. Diese Option ist für Linux-3.14 oder früher, oder wenn ein
           kleiner Geschwindigkeitsgewinn auf Kosten von möglicherweise viel
           Speicher gewünscht wird.
-l         Vermeidet die Benutzung von zramctl, selbst wenn verfügbar.
-k         Versucht nicht, ein vorheriges ZRAM unter diesem Device auszuhängen
           oder zu Deinitialisieren.

Falls push.sh im $PATH ist, kann man folgende sukzessive Optionen nutzen:
-K ARG     Übergibt ARG an den zugehörigen Aufruf von mkswap bzw. mkfs.*.
-M ARG     Übergibt ARG an den zugehörigen Aufruf von swapon/mount.
-2 ARG     Übergibt ARG an den Aufruf von tune2fs (nur für ext2 oder ext4).
-Z ARG     Übergibt ARG an den Aufruf von zramctl

Aufruf mit LANG=C schaltet die Übersetzungen aus. Aufruf: ${BASENAME} [Optionen] GRÖSSE|write [VERZEICHNIS] Kann ${devnode} nicht erzeugen: ${zclass} fehlt Kann ${block} nicht finden Kann Größe von zram${dev} nicht setzen Das Device ${dev} ist nicht numerisch Device-Anzahl ${num} ist nicht numerisch Fehler bei Berechnung der Größe Fehler bei Erzeugung von ${devnode} fehler beim Einschalten von writeback_limit für zram${dev} Fehler bei Idle Writeback von zram${dev} Fehler bei Rücksetzen von zram${dev} Fehler beim Setzen von writeback_limit ${writeback_limit} für zram${dev} Fehler beim Setzen von backing_dev für zram${dev} Fehler beim Setzen von comp_algorithm für  zram${dev} auf ${algo} Fehler beim Setzen von Idle Writeback für zram${dev} Fehler beim Setzen von unkomprimierbarem Writeback für zram${dev} Fehler beim setzten von zram${dev} max_comp_streams auf ${streams} Fehler beim Setzen von mem_limit für zram${dev} Fehler bei hot_add für ${devnode} mem_limit ${mem_limit} ist nicht numerisch Fehler bei mkfs.btrfs ${devnode} Fehler bei mkfs.ext2 ${devnode} Fehler bei mkfs.ext4 ${devnode} Fehler bei mkfs.xfs ${devnode} Fehler bei mkswap ${devnode} Fehler beim Einhängen von ${devnode} push.sh von https://github.com/vaeth/push/ (v2.0 oder neuer) benötigt Größe ${size} ist nicht numerisch Fehler bei swapon ${devnode} Dateisystem ${fstype} wird nicht unterstützt writeback_limit ${writeback_limit} ist nicht numerisch Fehler bei zramctl zram${dev} 