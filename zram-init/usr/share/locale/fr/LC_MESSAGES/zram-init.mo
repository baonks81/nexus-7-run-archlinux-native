��          �   %   �      p  g  q  -   �  +        3     H     c  "   �     �     �     �  $   �  2     7   K  "   �     �  %   �     �          #     ?     Z     s  D   �     �     �           &  �  @    �  ?   �#  6   +$     b$  0   �$  -   �$  7   �$     %  $   6%  -   [%  @   �%  N   �%  S   &  >   m&  !   �&  +   �&     �&     '     9'     X'     v'     �'  M   �'  &   �'     "(  ,   >(     k(                                                       	                                                      
              Prepare a zRAM device and use it as swap (resp. mount it under DIR).
SIZE is the maximal size in megabytes.
For umounting/freeing the zRAM device, use SIZE=0.
When using "write" (or anything else starting with "w") an idle writeback
is forced (only makes sense if previously initialized with "-w" or "-W LIMIT").
If DIR is - then only a filesystem is created in /dev/zram$DEV (or the device
is removed if SIZE is 0) but it is not mounted
(options "-o", "-c", "-m", and "-T" take no effect in this case, of course).
The latter can be useful e.g. for Btrfs if multiple devices should be mounted
jointly afterwards.
The following options are available.
An empty argument means the same as if the option is not specified.
-d DEV     Use zRAM device DEV. If not specified, DEV=0 is assumed.
           Make sure to use the matching value for umounting (SIZE=0)!
-D NUM     If modprobe needs to be used, require NUM devices. This is not
           recommended. Rely instead on /etc/modprobe.d/zram.conf with the line
           "options zram num_devices=NUM"
-s NUM     Use up to NUM parallel compression streams for the device
-S MAX     Use maximally MAX megabytes of uncompressed memory for the device
-b DEV     Use DEV as backing device
-I         If combined with "-b DEV", store incompressible pages to
           backing device
-w         If combined with "-b DEV", enable idle writeback to backing device
-W LIMIT   As "-w" but additionally set writeback_limit to LIMIT * 4kB.
-a ALGO    Set compression algorithm to ALGO (e.g. zstd, lz4, or lzo)
-c OWNER   If specified, chown to OWNER (resp. OWNER:GROUP) after mounting.
           If not specified, the default owner (root:root) is not changed.
-m MODE    If specified, chmod DIR to MODE after mounting.
-o OPTS    If specified, mount DIR with "-o OPTS".
-p PRIO    Use priority PRIO for the swap device.
           If not specified, PRIO=16383 is assumed.
           Use PRIO=- if you want to keep the default priority (-1).
-t TYPE    Use a filesystem of type TYPE if DIR is specified.
           TYPE can be either ext2, ext4, btrfs or xfs
           If not specified, TYPE=ext4 is assumed.
-B BSIZE   Override default blocksize (ext2, ext4) (BSIZE=1024|2048|4096)
-i IRATIO  If specified override default bytes/inodes in fs (ext2, ext4)
-N INODES  If specified override inode count (ext2, ext4)
-L LABEL   Specify the label LABEL for the new filesystem
-U UUID    Specify the uuid UUID for the new filesystem
-T         If specified, do not use the discard (TRIM) feature of ext4/swap.
           Use this option with linux-3.14 or earlier or when you want a slight
           speed increase at the cost of possibly wasting a lot of memory
-l         Do not use zramctl even if available
-k         Do no attempt to umount/free a previously used zRAM under this
           device

If you have push.sh in $PATH, you can also use accumulatively:
-K ARG     Pass ARG to the respective mkswap or mkfs.* call
-M ARG     Pass ARG to the respective swapon/mount call
-2 ARG     Pass ARG to the tune2fs call (ignored unless for ext2 or ext4)
-Z ARG     Pass ARG to the zramctl call

Call with LANG=C to disable translations Usage: ${BASENAME} [options] SIZE|write [DIR] cannot create ${devnode}: ${zclass} missing cannot find ${block} cannot set zram${dev} size device ${dev} is not numeric device count ${num} is not numeric failed to calculate size failed to create ${devnode} failed to reset zram${dev} failed to set zram${dev} backing_dev failed to set zram${dev} comp_algorithm to ${algo} failed to set zram${dev} max_comp_streams to ${streams} failed to set zram${dev} mem_limit hot_add failed for ${devnode} mem_limit ${mem_limit} is not numeric mkfs.btrfs ${devnode} failed mkfs.ext2 ${devnode} failed mkfs.ext4 ${devnode} failed mkfs.xfs ${devnode} failed mkswap ${devnode} failed mount ${devnode} failed push.sh from https://github.com/vaeth/push/ (v2.0 or newer) required size ${size} is not numeric swapon ${devnode} failed unsupported filesystem ${fstype} zramctl zram${dev} failed Project-Id-Version: zram-init
Report-Msgid-Bugs-To: Martin V"ath <martin@mvath.de>
PO-Revision-Date: 2020-07-26 08:49+0200
Last-Translator: Mattéo Rossillol‑‑Laruelle <beatussum@protonmail.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n >= 2);
X-Generator: Lokalize 19.12.3
 Prepare un périphérique zRAM et l'utilise comme swap (ou le monte au
chemin RÉPERTOIRE).
TAILLE est la taille maximale en mégaoctets.
Pour démonter/libérer le périphérique zRAM, utilisez TAILLE=0.
Lorsque vous utilisez « write » (ou n'importe quoi commençant par « w »)
an idle writeback est forcée (n'a de sens que si précédemment initialisé
par « -w » ou « -W LIMITE »).
Si RÉPERTOIRE est « - » alors le système de fichiers sera seulement créé
dans /dev/zram$PÉRIPH (ou le périphérique est supprimé si TAILLE
est égal à 0) et ne sera pas monté (les options « -o », « -c », « -m »
et « -T » n'ont donc aucun effet dans cette situation).
Le Btrfs peut être particulièrement utile si vous montez plusieurs
périphériques conjointement par la suite.
Les différentes options disponibles.
Un argument vide équivaut à l'absence dudit argument.
-d PÉRIPH     Utiliser le périphérique zRAM PÉRIPH. Si non spécifié, PÉRIPH=0.
              Veillez à utilisez la valeur adéquate lors du démontage (TAILLE=0) !
-D NUM        Si modprobe doit être utilisé, NUM périphériques sont requis. Ceci
              n'est pas recommandé. Reposez-vous plutôt sur /etc/modprobe.d/zram.conf
              à la ligne :
              « options zram num_devices=NUM »
-s NUM        Utiliser NUM flux parallèles de compression pour le périphérique
-S MAX        Utiliser un maximum MAX (en mégaoctets) de mémoire non compressée
              pour le périphérique
-b PÉRIPH     Utiliser PÉRIPH comme périphérique de support
-I            Si combiné avec « -b PÉRIPH », stocke les pages incompressible dans le
              périphérique de support
-w            Si combiné avec « -b PÉRIPH », enable idle writeback to backing device
-W LIMITE     Comme « -w » mais spécifie en plus la limite d'écritures différées à
              LIMITE * 4ko.
-a ALGO       Spécifier l'algorithme de compression à ALGO (c'est-à-dire zstd, lz4,
              ou lzo)
-c PROPRIO    Si spécifié, change le propriétaire à PROPRIO (de la même façon pour
              PROPRIO:GROUPE) après le montage.
              Si non spécifié, le propriétaire par défaut (root:root) reste inchangé.
-m MODE       Si spécifié, change les permissions d'accès de RÉPERTOIRE à MODE
              après le montage.
-o OPTS       Si spécifié, monter le RÉPERTOIRE avec « -o OPTS »
-p PRIO       Utiliser une priorité PRIO pour l'espace d'échange.
              Si non spécifié, PRIO=16383.
              Utilisez PRIO=- si vous souhaitez garder la priorité par défaut (-1).
-t TYPE       Utiliser un système de fichiers de type TYPE si RÉPERTOIRE est spécifié
              également.
              TYPE peut être soit ext2, ext4, btrfs ou xfs
              Si non spécifié, TYPE=ext4.
-B BSIZE      Surcharge la taille de bloc (ext2 et ext4) (BSIZE=1024|2048|4096)
-i IRATIO     Si spécifié surcharge le ration octet/inodes par défaut (ext2 et ext4)
-N INODES     Si spécifié surcharge le nombre d'inodes (ext2 et ext4)
-L ÉTIQUETTE  Spécifier l'ÉTIQUETTE du nouveau système de fichiers
-U UUID       Spécifier le U.U.I.D. du nouveau système de fichiers
-T            Si spécifié, ne pas utiliser la fonctionnalité discard (TRIM) de ext4 et swap.
              Utilisez cette option avec un noyau Linux 3.14 ou antérieure ou lorsque vous
              souhaitez une légère augmentation de vitesse aux frais d'un possible gâchis
              important de mémoire
-l            Ne pas utiliser zramctl même si disponible
-k            Ne pas essayer de démonter/libérer une zRAM précédemment utilisée par ce
              périphérique

Si push.sh est présent dans $PATH, vous pouvez également utiliser :
-K ARG        Passer ARG à l'appel de mkswap ou mkfs.*
-M ARG        Passer ARG à l'appel de swapon ou mount
-2 ARG        Passer ARG à l'appel de tune2fs (ignoré sauf pour ext2 et ext4)
-Z ARG        Passer ARG à l'appel de zramctl

Appelez avec LANG=C pour désactiver les traductions Utilisation : ${BASENAME} [options] TAILLE|write [RÉPERTOIRE] impossible de créer ${devnode} : ${zclass} manquante impossible de trouver ${block} impossible de spécifier la taille de zram${dev} le périphérique ${dev} n'est pas numérique le nombre de périphérique ${num} n'est pas numérique échec du calcul de la taille échec de la création de ${devnode} la réinitialisation de zram${dev} a échoué la valeur backing_dev de zram${dev} n'a pas pu être spécifiée la valeur comp_algorithm de zram${dev} n'a pas pu être spécifiée à ${algo} la valeur max_comp_streams de zram${dev} n'a pas pu être spécifiée à ${streams} la valeur mem_limit de zram${dev} n'a pas pu être spécifiée échec de hot_add pour ${devnode} mem_limit ${mem_limit} n'est pas numérique échec de mkfs.btrfs ${devnode} échec de mkfs.ext2 ${devnode} échec de mkfs.ext4 ${devnode} échec de mkfs.xfs ${devnode} échec de mkswap ${devnode} échec de mount ${devnode} push.sh venant de https://github.com/vaeth/push/ (v2.0 ou ultérieure) requis la taille ${size} n'est pas numérique échec de swapon ${devnode} système de fichiers ${fstype} non supporté échec de zramctl zram${dev} 